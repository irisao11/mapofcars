import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapOfCarsTest {
    public MapOfCarsTest() {
    }

    @Test
    public void MapOfCarTest(){
        MapOfCars mapOfCars = new MapOfCars();
        Map<String, List<String>> myResult = mapOfCars.createMapOfCars();
        Map<String, List<String>> myTest = new HashMap<>();
        myTest.put("albastru", Arrays.asList("BMW","Mercedes"));
        myTest.put("rosu", Arrays.asList("Audi","Citroen"));
        myTest.put("negru", Arrays.asList("Volvo","Renault"));

        Assert.assertEquals(myTest, myResult);
    }
}
