import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CarReader {

    public List<Car> carList(String filePath) {
        List<Car> carList = new LinkedList<>();

        try {
            FileReader reader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line = bufferedReader.readLine();

            while (line != null) {
                String[] elem = line.split(" ");
                Car car = new Car();
                car.setName(elem[0]);
                car.setColour(elem[1]);
                car.setYear(elem[2]);

                carList.add(car);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return carList;
    }
}
