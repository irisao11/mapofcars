import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapOfCars {

    public Map<String, List<String>> createMapOfCars() {

        CarReader carReader = new CarReader();
        List<Car> myList = carReader.carList(Resourses.CALEA_CATRE_FISIERE_1);

        Map<String, List<String>> myMapOfCars = new HashMap<String, List<String>>();
        for (Car item : myList) {
            if (!myMapOfCars.containsKey(item.getColour())) {
                myMapOfCars.put(item.getColour(), new ArrayList<String>());
            }
            myMapOfCars.get(item.getColour()).add(item.getName());
        }
        return myMapOfCars;
    }
}
