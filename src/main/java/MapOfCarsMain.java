import java.util.List;
import java.util.Map;

public class MapOfCarsMain {
    public static void main(String[] args) {

        MapOfCars mapOfCars = new MapOfCars();
        Map<String, List<String>> myMapOfCars = mapOfCars.createMapOfCars();

        for (Map.Entry<String, List<String>> entry : myMapOfCars.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }

    }
}
